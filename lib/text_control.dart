import 'package:flutter/material.dart';

import './text_output.dart';

class TextControl extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TextControlState();
  }
}

class _TextControlState extends State<TextControl> {
  String _mainText = 'This is the first assignment 11 !';

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            width: double.infinity,
            height: 400,
            child: Card(
            margin: EdgeInsets.all(20),
            child: Column(
              children: [
                RaisedButton(
                  onPressed: () {
                    setState(() {
                      _mainText = 'This changed!';
                    });
                  },
                  child: Text('Change Text'),
                ),
                TextOutput(_mainText)
              ],
            ))));
  }
}
